[requires]
gtest/1.10.0

[generators]
cmake
visual_studio

[options]
gtest:build_gmock=False