#include <gtest/gtest.h>
#include "calculate/calculator.h"

TEST(Calculate, ValidCases) {
    EXPECT_FLOAT_EQ(calculate("1+1"), 2);
    EXPECT_FLOAT_EQ(calculate("2.5+1.3"), 3.8f);
    EXPECT_FLOAT_EQ(calculate(" 2.5 + 1.3 "), 3.8f);
}

TEST(Calculate, InvalidCases) {
    EXPECT_ANY_THROW(calculate("1"));
    EXPECT_ANY_THROW(calculate("1.2+a"));
    EXPECT_ANY_THROW(calculate("1.4*4.5"));
    EXPECT_ANY_THROW(calculate("1.4-4.5"));
}