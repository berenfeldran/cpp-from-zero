#include <string>
#include "calculate/calculator.h"

float calculate(const std::string& formula)
{
    const size_t plus_sign_pos = formula.find('+');
    if (plus_sign_pos == std::string::npos) {
        throw std::string("+ sign was not found in formula '" + formula + "'");
    }
    const std::string term1 = formula.substr(0, plus_sign_pos);
    const std::string term2 = formula.substr(plus_sign_pos + 1);

    return std::stof(term1) + std::stof(term2);
}