#include <iostream>
#include <stdlib.h> 
#include "calculate/calculator.h"

int main(int argc, char** argv) {
    std::string formula;
    for (int i = 1; i < argc; ++i)
    {
        formula += argv[i];
    }
    try {
        std::cout << calculate(formula) << std::endl;
    } catch (...) {
        std::cerr << "Invalid formula '" << formula << "'" << std::endl;
        exit(1);
    }
}
