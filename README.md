# c++ project - from zero to hero

## Introduction

This is an online course about how to build modern c++ project, with extensive CI/CD and cross platform capabilities using gitlab.com platform.
The course is organized in lessons. Each lesson is a commit in this repository.

### Table of contents

- [c++ project - from zero to hero](#c-project---from-zero-to-hero)
  - [Introduction](#introduction)
    - [Table of contents](#table-of-contents)
    - [Target audience](#target-audience)
    - [Pre-requisites](#pre-requisites)
    - [About the author](#about-the-author)
    - [Contributions](#contributions)
  - [Lesson 1 : simple c++ project code & skeleton, and cmake build](#lesson-1--simple-c-project-code--skeleton-and-cmake-build)
    - [Adding a simple c++ program](#adding-a-simple-c-program)
    - [Building our c++ program with cmake, make and g++](#building-our-c-program-with-cmake-make-and-g)
    - [Defining a project skeleton](#defining-a-project-skeleton)
  - [Lesson 2 : simple CI with gitlab](#lesson-2--simple-ci-with-gitlab)
    - [Creating CI pipeline in gitlab with the .gitlab-ci.yml file](#creating-ci-pipeline-in-gitlab-with-the-gitlab-ciyml-file)
  - [Lesson 3 : create a build environment](#lesson-3--create-a-build-environment)
    - [Defining a Dockerfile](#defining-a-dockerfile)
    - [Building the build environment docker image](#building-the-build-environment-docker-image)
    - [Developing with Visual Studio Code inside a docker container](#developing-with-visual-studio-code-inside-a-docker-container)
  - [Lesson 4 : extending our c++ program](#lesson-4--extending-our-c-program)
    - [using multiple source and header files](#using-multiple-source-and-header-files)
    - [using cmake with GLOB](#using-cmake-with-glob)
  - [Lesson 5 : Adding unit tests with gtest](#lesson-5--adding-unit-tests-with-gtest)
    - [Adding GTest to our build environment](#adding-gtest-to-our-build-environment)
    - [Writing unit tests with gtest](#writing-unit-tests-with-gtest)
    - [Dividing our code into an executable and a library](#dividing-our-code-into-an-executable-and-a-library)
    - [Building the GTest test executable with cmake](#building-the-gtest-test-executable-with-cmake)
    - [Running the GTest unit tests as part of the gitlab pipeline](#running-the-gtest-unit-tests-as-part-of-the-gitlab-pipeline)
  - [Lesson 6 : Automated build of build environment](#lesson-6--automated-build-of-build-environment)
    - [When should we build our build environment image](#when-should-we-build-our-build-environment-image)
  - [Lesson 7 : cmake with sub-directories](#lesson-7--cmake-with-sub-directories)
  - [Lesson 8 : adding cmake options and gitlab CI conditions](#lesson-8--adding-cmake-options-and-gitlab-ci-conditions)
  - [Lesson 9 : using ccache](#lesson-9--using-ccache)
  - [Lesson 10 : use conan for external libraries](#lesson-10--use-conan-for-external-libraries)
  - [Lesson 11 : build on windows](#lesson-11--build-on-windows)

### Target audience

* Dev-Ops engineers, especially in projects with extensive c++ developement, that would like to improve the performance and productivity of the projects
* Senior c++ developers, especially the ones leading small teams or startup teams, that are looking for high performance, fast paced c++ projects. 

### Pre-requisites

This course level is somewhere between intermidiate and advanced. You should get yourself familiered with a few topics before starting it. We will assume that you have some knowledge in experties in the listed material. Here is the list, with some recommended tutorials:

* git - there are many great tutorials out there [such as this one](https://www.youtube.com/watch?v=8JJ101D3knE)
* gitlab - basic understanding of gitlab concepts such as repositories, pipelines, runners, etc is also required. [Here is a good tutorial to start with](https://www.youtube.com/watch?v=Jt4Z1vwtXT0&list=PLhW3qG5bs-L8YSnCiyQ-jD8XfHC2W1NL_)
* c++ - is required here at a basic to intermidiate level. Including usage of stl containers such as std::string and std::vector. 
* docker - we will be using docker containers, images, repositories and registries, files and more all around. [Here is an example docker tutorial](https://www.youtube.com/watch?v=fqMOX6JJhGo)

### About the author

My name is Ran Berenfeld and I am a senior c++ developer. I am leading a c++ focused engineering team in a young, cutting edge israeli startup.

### Contributions

Contributions and comments are welcomed. 

## Lesson 1 : simple c++ project code & skeleton, and cmake build

In this stage we will start off with a simple c++ console application, a simple cmake build procedure and some project skeleton.
These are some basic foundations to get us going. We will later extend both the c++ program and the cmake build system for more advanced use cases.

### Adding a simple c++ program

We will start off our journey by writing a very c++ console application. In this case, a program that takes
two arguments and prints out their sum. In this lecture we will add the program itself, and see how to build and run it using 
cmake in Ubuntu platform.

**src/main.cpp**
```cpp
#include <iostream>
#include <stdlib.h> 

int main(int argc, char** argv) {
    if (argc < 3) {
        std::cerr << "Usage: ConsoleCalculator [a] [b]" << std::endl;
        return 1;
    }
    float a = std::atof(argv[1]);
    float b = std::atof(argv[2]);
    
    std::cout << "Calculating " << a << " + " << b << " = " << (a+b) << std::endl;
}
```

### Building our c++ program with cmake, make and g++

Now lets build this simple program using in linux using `cmake` and a c++ compiler in `Ubuntu 18.04` OS.
cmake is the standard tool to build C++ projects. It is cross platform - For example, it can generate Makefile for linux
build using `make`, or visual studio solutions to build on MS windows using MSVC.

For cmake we need to create a `CMakeLists.txt` file that will tell cmake what to build. We will start with a very simple
CMakeLists.txt file that will build a binary called `ConsoleCalculator` from a single source file `src/main.cpp`

**CMakeLists.txt**
```cmake
cmake_minimum_required(VERSION 3.0.0)
project(ConsoleCalculator)

add_executable(ConsoleCalculator src/main.cpp)
```

Now, in order to build this code with cmake, we can do the following:

* `sudo apt install cmake make g++` - to install cmake, make, and g++ compiler.
* `mkdir -p build; cd build; cmake ..; make` - to create and enter a fresh build directory, run cmake to generate a
a `Makefile` in the source root directory and build it using make. 

Now our built binary is waiting at `build/ConsoleCalculator` and we can run it. Here is the output :

```sh
09:14:18 /home/ran/personal/cpp-from-zero (master) ./build/ConsoleCalculator 3.5 6.7
Calculating 3.5 + 6.7 = 10.2
09:14:38 /home/ran/personal/cpp-from-zero (master) 
```

### Defining a project skeleton

Lets also build a basic skeleton for our project. We will add designate `src` folder for source files,
and `include` folder for header files. 
It is important to define ahead where everything will go. Remember to add `.gitignore` if you want to add empty folders to `git`.

```sh
├── CMakeLists.txt
├── include
├── README.md
└── src
    └── main.cpp
```

## Lesson 2 : simple CI with gitlab

In this stage we will implement a basic CI, that will check that the project can be built and launched on each commit.

### Creating CI pipeline in gitlab with the .gitlab-ci.yml file

`Continous Integration (CI)` means continuosly merging developers code into our project, and for it to work efficiently we must
build a CI platform that will automatically verify every change done by a developer before merging it in.
Using `Gitlab` we achieve this by defining a `pipeline` that will run on every push to this gitlab repository.
To define such a pipeline, we edit and commit a `.gitlab-ci.yml` file. 
Let's add a simple .gitlab-yml file that will build the ConsoleCalculator and will do an example run of it.

```yml
build-linux:
  stage: build
  image: ubuntu:18.04
  script:
    - apt-get update
    - apt-get install -y --no-install-recommends cmake make g++
    - mkdir -p build
    - cd build
    - cmake ..
    - make
  artifacts:
    paths:
      - build

test-linux:
  variables:
    GIT_STRATEGY: none
  stage: test
  image: ubuntu:18.04
  script:
    - ./build/ConsoleCalculator 3.5 4.6
  needs:
    - job: "build-linux"
```

Obviously if you are new to it you need to study the .gitlab-yml file syntax. [This is the .gitlab-yml reference in gitlab.com](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html). Here are a few explanations about this file :

* Pipelines are divided into `stages`, and each stage has jobs. In this case we have two stages - `build` and `test`
* Each stage has `jobs`. In this case - build-linux and test-linux jobs.
* The `image:` tag on each job specify on which docker image to run this job. By default, gitlab uses docker executors to execute the `script:` content, and this is the base image to start with.
* The `build-linux` job installs cmake,make and g++ and then builds the project. we saw it in stage 1.
* Each job can have dependencies. one way to define it is with the `needs:` keyword. In this case, `test-linux` job will not start until `build-linux` job is done.
* Each job can have artifacts. These are stored for a period of time in the gitlab server. The important thing to note is that when a job starts, the artifacts from all the jobs it depends on are copied to its working directory as well. This allows to run `./build/ConsoleCalculator`, which is part of the artifact of build-linux job, in the test-linux job
* by default, gitlab will clone the repository (and will check out the commit for which the pipeline is running) for each job. However, the `GIT_STRATEGY: none` variable in test-linux job will tell gitlab to skip this step for test-linux job. This is on purpose. The test-linux job wants to verify the built binaries from the previous build-linux job on a "clean" system.

In the repository settings, we can define `master` as a protected branch, and we can define that all pipelines must pass in order to merge a `merge request` to master. Once we do it we have achieved a great first goal - a code that does not compile will fail the make command above, and as such will fail the pipeline, and will not be able to merge in to master. This is how such CI pipeline protects our master branch

## Lesson 3 : create a build environment

In this stage we will define our build environment, and show how we can work inside a running docker container.
The main benefit of this approach is that the build environment becomes defined, stable, reproducible and cloneable. 
After we employ this step we can easy create fresh working environment for our developers, and anything installed on the developer machine
will no longer interrupt. They will all use the same version of build toolset, and will produce the same binaries.

### Defining a Dockerfile

We will maintain our `Dockerfile` in the git repository. We will start with this basic docker file.

**Dockerfile**
```dockerfile
FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y --no-install-recommends make cmake g++ git

ENTRYPOINT /bin/bash
```

Lets go over this docker file and see what will happen when we will build an image from it
* We will start from the official ubuntu:18.04 image in dockerhub
* we will install make, cmake, g++ and git. the first 3 are mandatory to build the project. 
and git will be very usefull when we will work inside the container
* The `ENTRYPOINT` tells docker to run a bash in a terminal to start this container. This terminal will be the main process of
the docker container. If it will be terminated, the container will be shut down.

### Building the build environment docker image

Using the `docker build` command we will build the image:

```sh
09:59:09 /home/ran/personal/cpp-from-zero (master) docker build -t cpp-from-zero-build-env .
Sending build context to Docker daemon  731.1kB
Step 1/4 : FROM ubuntu:18.04
 ---> 62337d0c6de2
Step 2/4 : RUN apt-get update
 ---> Using cache
 ---> ebf468b3196c
Step 3/4 : RUN apt-get install -y --no-install-recommends make cmake g++ git
 ---> Using cache
 ---> 18a8a3f9115c
Step 4/4 : ENTRYPOINT /bin/bash
 ---> Using cache
 ---> c1ee0f3396ba
Successfully built c1ee0f3396ba
Successfully tagged cpp-from-zero-build-env:latest
10:02:04 /home/ran/personal/cpp-from-zero (master) 
```

we named our image `cpp-from-zero-build-env` and we can see it there if we run `docker images`. Now lets start a container from the image,
that can be used to develope "inside" a running containuer:

```sh
10:06:28 /home/ran/personal/cpp-from-zero (master) docker run -v $PWD:/src -td --name my-build-env cpp-from-zero-build-env
12094919915b2c8eaeffebe09bb53fe39ec6d305835fe30e80e1fa6130eb0c60
10:06:40 /home/ran/personal/cpp-from-zero (master)
```

Now the container is running. we mapped our current working directory (`$PWD`), the root of the project, to `/src` folder inside the
containuer and we named it `my-build-env`. We should see the container when we run `docker ps`

```sh
10:06:40 /home/ran/personal/cpp-from-zero (master) docker ps
CONTAINER ID   IMAGE                     COMMAND                  CREATED         STATUS        PORTS     NAMES
12094919915b   cpp-from-zero-build-env   "/bin/sh -c /bin/bash"   2 seconds ago   Up 1 second             my-build-env
10:06:41 /home/ran/personal/cpp-from-zero (master) 
```

Our build environment is defined now. In the future we will even see that part of the CI can be to build it and make sure
it is functional and up to date.

### Developing with Visual Studio Code inside a docker container

We will see now how to develope inside the container we just created using Visual Studio Code. You can find more information
on this topic in the [Developing inside a Container](https://code.visualstudio.com/docs/remote/containers) page.

In Visual Studio Code, everything works with extensions. After downloading and installing it, you need to install the [Remote-Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) extension. Then you can click ctrl-shit-p and search and select
`Remote-Containers: Attach tp Running Container`. Given that the container above is running, you can select it (use the name we gave it `my-build-env`). Then, another instance of Visual Studio Code will be opened. This one is running inside the container. Now select File -> Open folder and open the `/src`
folder we mapped above.

Visual Studio Code has another extension, [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools) that is very recommended here as it can automatically build the project from the IDE with our `CMakeLists.txt` file. Install it, it will automatically configure
the project. It may ask you for a compiler to choose. You can use GCC 7.5.0 to compile the project with g++. 

![Visual Studio Code with CMake and Remote Containers extensions](docs/lecture_3.png?raw=true "Visual Studio Code with CMake and Remote Containers extensions")
 
## Lesson 4 : extending our c++ program

This is a good time to extend our c++ program a bit. First, we would like to add source and header files, and use cmake to build them. 
We would also add a function that will calculate the result from the input string. This will make it easier for us to add unit tests for
this function later on.

### using multiple source and header files

We will use the `/src` folder for source files and `/include` folder for header files. Here is the updated directory structure:

```sh
16:06:01 /home/ran/personal/cpp-from-zero (master) tree
.
├── CMakeLists.txt
├── Dockerfile
├── include
│   └── calculator.h
├── README.md
└── src
    ├── calculator.cpp
    └── main.cpp

2 directories, 6 files
16:06:25 /home/ran/personal/cpp-from-zero (master) 
```

Now we added `src/calculator.cpp` source file and `include/calculator.h` header file to define a function that will perform the calculation:

**include/calculate.h**
```cpp 
float calculate(const std::string& formula);
```

**src/calculate.cpp**
```cpp
#include <string>

float calculate(const std::string& formula)
{
    const size_t plus_sign_pos = formula.find('+');
    if (plus_sign_pos == std::string::npos) {
        throw std::string("+ sign was not found in formula '" + formula + "'");
    }
    const std::string term1 = formula.substr(0, plus_sign_pos);
    const std::string term2 = formula.substr(plus_sign_pos + 1);

    return std::stof(term1) + std::stof(term2);
}
```

And in our main program we will concatanate all the input arguments into a single string that will be sent to the `calculate` method for evaluation.

**src/main.cpp**
```cpp
#include <iostream>
#include <stdlib.h> 
#include "calculator.h"

int main(int argc, char** argv) {
    std::string formula;
    for (int i = 1; i < argc; ++i)
    {
        formula += argv[i];
    }
    try {
        std::cout << calculate(formula) << std::endl;
    } catch (...) {
        std::cerr << "Invalid formula '" << formula << "'" << std::endl;
        exit(1);
    }
}
```

Note that the calculate function may throw some exceptions, if things are not parsed well. We will see later how to add unit tests for this function.

### using cmake with GLOB

we will update our cmake build to add an include folder and add all .cpp files in the src folder:

**CMakeLists.txt**
```cmake
cmake_minimum_required(VERSION 3.0.0)
project(ConsoleCalculator)

file(GLOB ConsoleCalculator_Srcs ${PROJECT_SOURCE_DIR}/src/*.cpp)
add_executable(ConsoleCalculator ${ConsoleCalculator_Srcs})
target_include_directories(ConsoleCalculator PRIVATE include)
```

Now we can safely add cpp files to the src folder and header files to the include folder.

## Lesson 5 : Adding unit tests with gtest

`GTest` stands for `googletest` and is a very common library to write unit tests in c++. 
However, adding a 3rd party library, or external library, is never a simple task. In our example, it involves updating our build environment, cmake file, and even refactor our code a bit. So this change will require changes in some files. Lets get to work.

### Adding GTest to our build environment

This can be done in several ways. GTest is an external c++ library, so usually we have to get some header files to include, and some object files or shared objects (`.a` files or `.so` files in linux) to link with. Lets use the `libgtest-dev` packages, available for Ubuntu, to install the library. The package will only download the library source files, so we will have to build it. It is actually quite easily - with cmake and make like our own project. Lets see how this is done in our docker file

**Dockerfile**
```dockerfile
FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y --no-install-recommends make cmake g++ git
RUN apt-get install -y --no-install-recommends libgtest-dev

RUN cd /usr/src/gtest && cmake . && make && cp *.a /usr/lib && cd -

ENTRYPOINT /bin/bash
```

The only change we added is those 2 lines `RUN apt-get install -y --no-install-recommends libgtest-dev` and `RUN cd /usr/src/gtest && cmake . && make && cp *.a /usr/lib && cd -`. The first one installes libgtest-dev, the other one builds it. The build product are some object files, `libgtest.a` and `libgtest_main.a` that we will need to link with. Copying these objects files to `/usr/lib` will enable us to just specfiy link libraries in the cmake file, but this is a matter of personal choice.

### Writing unit tests with gtest

Now lets add a `tests` folder for our tests and our first test file.

**testCalculate.cpp**
```cpp
#include <gtest/gtest.h>
#include "calculator.h"

TEST(Calculate, ValidCases) {
    EXPECT_FLOAT_EQ(calculate("1+1"), 2);
    EXPECT_FLOAT_EQ(calculate("2.5+1.3"), 3.8f);
    EXPECT_FLOAT_EQ(calculate(" 2.5 + 1.3 "), 3.8f);
}

TEST(Calculate, InvalidCases) {
    EXPECT_ANY_THROW(calculate("1"));
    EXPECT_ANY_THROW(calculate("1.2+a"));
    EXPECT_ANY_THROW(calculate("1.4*4.5"));
    EXPECT_ANY_THROW(calculate("1.4-4.5"));
}
```

These are some basic positive and negative tests on the `calculate` function, that either expect a return value or an error to be thrown. 
To run the unit tests, we simply execute it. Here is a sample output:

```sh
21:19:06 /home/ran/personal/cpp-from-zero (create-readme) ./build/testCalculate 
Running main() from gtest_main.cc
[==========] Running 2 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 2 tests from Calculate
[ RUN      ] Calculate.ValidCases
[       OK ] Calculate.ValidCases (0 ms)
[ RUN      ] Calculate.InvalidCases
[       OK ] Calculate.InvalidCases (0 ms)
[----------] 2 tests from Calculate (0 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 1 test case ran. (1 ms total)
[  PASSED  ] 2 tests.
21:19:10 /home/ran/personal/cpp-from-zero (create-readme) 
```

### Dividing our code into an executable and a library

Every c++ code that we want to run needs to be linked to an executable. This means that if we want, for example, to test the `calculate` function defined in `src/calculator.cpp`, we will have to link to our test program the compiled object of `src/calculator.cpp`. One way would be to add the source file to both our `ConsoleCalculator` executable and to our test executible, however this will involve having a source file "belonging" to multiple targets.

A much cleaner way is to define a library that will expose the `calculate` function in its public API. Lets see how our `CMakeLists.txt` file looks now.

The library `CalculateLib` that is defined with the `add_library` cmake command can be either a static library or a shared object. It is used both by the `ConsoleCalculator` executable and by a test executable we created, `testCalculate`

### Building the GTest test executable with cmake

To build the `testCalculate` exeutable with cmake, we need to add to it include directories and link libraries. One way to do it is with the `find_package` cmake command. For many common
package it exists. It looks for the package header files and object files and fills in some cmake variables that can be used. In this case we used the `${GTEST_INCLUDE_DIRS}` that contains the GTest include directory and `${GTEST_MAIN_LIBRARIES} ${GTEST_LIBRARIES}` that contains gtest link libraries. (${GTEST_LIBRARIES} contains the gtest implementation, while{GTEST_MAIN_LIBRARIES} library contains a simple main() that runs gtest, so that we dont have to add a main() ourselves). 

So here is how the cmake file looks like, now dividing it into a library and using gtest:

**CMakeLists.txt**
```cmake
cmake_minimum_required(VERSION 3.0.0)
project(ConsoleCalculator)

# CalculateLib library
file(GLOB CalculateLib_Srcs ${PROJECT_SOURCE_DIR}/src/calculator.cpp)
add_library(CalculateLib STATIC ${CalculateLib_Srcs})
target_include_directories(CalculateLib PUBLIC include)

# ConsoleCalculator executable
file(GLOB ConsoleCalculator_Srcs ${PROJECT_SOURCE_DIR}/src/main.cpp)
add_executable(ConsoleCalculator ${ConsoleCalculator_Srcs})
target_link_libraries(ConsoleCalculator PRIVATE CalculateLib)

# testCalculate testing executable
find_package(GTest REQUIRED)
add_executable(testCalculate ${PROJECT_SOURCE_DIR}/tests/testCalculate.cpp)
target_include_directories(testCalculate PRIVATE ${GTEST_INCLUDE_DIRS})
target_link_libraries(testCalculate PRIVATE CalculateLib pthread ${GTEST_MAIN_LIBRARIES} ${GTEST_LIBRARIES})
```

### Running the GTest unit tests as part of the gitlab pipeline

In the gitlab pipeline definition, we did these modifications:

* we updated the build job to pre-install gtest
* we updated the test job to run the unit tests.

The GTest unit tests integrate well into gitlab. You can define an artifact xml report that can show in gitlab the list of tests that passed or failed.

Here is the updated gitlab CI definition file:

**.gitlab-ci.yml**
```yml
build-linux:
  stage: build
  image: ubuntu:18.04
  script:
    - apt-get update
    - apt-get install -y --no-install-recommends cmake make g++ libgtest-dev
    - cd /usr/src/gtest && cmake . && make && cp *.a /usr/lib && cd -
    - mkdir -p build && cd build && cmake .. && make && cd -
  artifacts:
    paths:
      - build

test-linux:
  variables:
    GIT_STRATEGY: none
  stage: test
  image: ubuntu:18.04
  script:
    - ./build/testCalculate --gtest_output=xml:build/testCalculate.xml
  needs:
    - job: "build-linux"
  artifacts:
    when: always
    reports:
      junit: build/testCalculate.xml
```
## Lesson 6 : Automated build of build environment

If you followed closely the last lesson, you might have noticed that this line appears twice in the source tree :
```
cd /usr/src/gtest && cmake . && make && cp *.a /usr/lib && cd -
```
This is because it belongs to both the `Dockerfile` that is used to build the build environment, and to the `.gitlab-ci.yml` file, describing
for gitlab how to install the build environment on top of a basic `ubuntu:18.04` container.

So we have a few problems with this solution:
* the build environment is built every time we run a CI pipeline
* the build environment is defined twice
* a developer that would like to work in a container will have to build the build environment locally

To improve this, we would like to build our build environment docker image as part of the build process, but then we would need a location to "store" the built docker image. such location is called a docker registry. luckily for us, gitlab comes with a personal docker registry for each project. and on top of it - gitlab CI jobs are run with a special environment variables to login (using `docker login`) to the gitlab docker registry and push the docker image (using `docker push`). So lets see how it is done. the Dockerfile is unchanged. Lets see the gitlab-ci.yml :

```yml
stages:
  - build_docker_images
  - build
  - test

build-linux-build-env:
  stage: build_docker_images
  image: docker:19.03.12
  services:
    - docker:19.03.13-dind
  script:
    - docker login $CI_REGISTRY_IMAGE --username $CI_REGISTRY_USER --password "$CI_REGISTRY_PASSWORD"
    - docker build -t $CI_REGISTRY_IMAGE/cpp-from-zero-build-env .
    - docker push $CI_REGISTRY_IMAGE/cpp-from-zero-build-env

build-linux:
  stage: build
  image: $CI_REGISTRY_IMAGE/cpp-from-zero-build-env
  script:
    - mkdir -p build && cd build && cmake .. && make && cd -
  artifacts:
    paths:
      - build
  needs:
    - job: "build-linux-build-env"

# [ommited]
```

Lets go over the `build-linux-build-env:` job that we defined

* The image is `docker:19.03.12` . this is a special image that comes with docker installed. The service 'docker:19.03.13-dind' that is started will run the docker daemon service inside the container. When you do docker build, docker needs a local docker daemon to write the image to. Later, docker push will push the image to a remote registry.
* The command `docker login $CI_REGISTRY_IMAGE --username $CI_REGISTRY_USER --password "$CI_REGISTRY_PASSWORD"` is performing authentication against the gitlab registry. Gitlab pre-defined environment are extensivly used here. you can checkout [gitlab predefined environment page](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) to see exactly which variables are defined.
* The next command `docker build -t $CI_REGISTRY_IMAGE/cpp-from-zero-build-env .` is building the Dockerfile, present in the repository in the current folder, and tagging it ahead with the gitlab registry. Every docker image has a name that contains a registry, repository and a tag. When we tag the image we just give it another name, so that we can push it to that registry. we could have achieved the same effect using `docker tag` command after docker build.
* The last command `docker push $CI_REGISTRY_IMAGE/cpp-from-zero-build-env` is pushing the built image to gitlab registry

Now comes the great part. the image that we use on the `build-linux` job is no longer ubuntu. it is now `$CI_REGISTRY_IMAGE/cpp-from-zero-build-env`, and is now ready with everything that we need to build, and the gtest, 3rd party library. this makes the build job faster. This also makes the docker image available to developers.

### When should we build our build environment image

This is a good question. A build environment does not change often like code. It usually changes when we add, or change versions, of build tools, or external libraries that we use. So we might want to save our time and not build it on every commit, like we are doing here. A few other options can be

* build the build environment in the CI pipeline only on a manual trigger, like a custom pipeline variable.
* build the build environment only as a response to changes in the Dockerfile. This may be risky : for example we may be using libraries with custom code changes, and changes to these libraries will not reflect changes to the build environment.
* build the build environment on every tag or release. Usually with the release version in the docker image tag. The good thing here is that we maintain a build environment for every release. If we employ this strategy we should make sure that only a single change to the build environment is introduced in each release, to make a build environment reliable for all the commits within a release.

In this lesson we will keep it as is for now. Later, we might see a better approach.
* 
## Lesson 7 : cmake with sub-directories

A complex c++ project can have quite a few libraries and binaries. In this example, we already have a library called `CalculateLib` and two binaries : the main program binary called `ConsoleCalculator` and a test binary called `testCalculate`. However, only the testCalculate binary really needs the `gflags`package. Furthermore, our main `CMakeLists.txt` already grew quite big. 
In this lesson, we will split the main CMakeLists.txt file into sub directories. This way we can define each library and binary in each own cmake file, with its dependencies. This is the revised directory structure:

```sh
18:46:28 /home/ran/personal/cpp-from-zero (lesson_7) tree -d
.
├── include
│   └── calculate
├── src
│   ├── calculate
│   └── console-calculator
└── tests

6 directories
18:46:34 /home/ran/personal/cpp-from-zero (lesson_7) 
```

so we split the `src` folder to subfolders with targets, `calculate` folder with the `CalaulateLib` folder, and `console-calculator` folder with the `ConsoleCalculator` binary. Each folder has its own CMakeLists.txt file. And the main cmake files have `add_subdirectory` commands to include subfolders. Lets see some example, updated, cmake files:

**CmakeLists.txt**
```cmake
cmake_minimum_required(VERSION 3.0.0)
project(ConsoleCalculator)

add_subdirectory(src)
add_subdirectory(tests)
```

**src/CMakeLists.txt**
```cmake
add_subdirectory(calculate)
add_subdirectory(console-calculator)
```

**src/calculate/CMakeLists.txt**
```cmake
# CalculateLib library
file(GLOB CalculateLib_Srcs ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)
add_library(CalculateLib STATIC ${CalculateLib_Srcs})
target_include_directories(CalculateLib PUBLIC ${CMAKE_SOURCE_DIR}/include)
```

**src/console-calculator/CMakeLists.txt**
```cmake
# ConsoleCalculator executable
file(GLOB ConsoleCalculator_Srcs ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)
add_executable(ConsoleCalculator ${ConsoleCalculator_Srcs})
target_link_libraries(ConsoleCalculator PRIVATE CalculateLib)
```

## Lesson 8 : adding cmake options and gitlab CI conditions

The user may want to opt out unit testing. It is very common to add cmake options to build part of the project. We will also add a gitlab variable with the same name, so that the user is able to run a build pipeline without building or running unit tests.

It is best to have some project prefix for our variables, to avoid colliding with many other pre defined cmake and gitlab environment variables.
Here is how we use a new `CCALC_BUILD_TESTS` variable in the cmake file and in the gitlab pipeline

**CMakeLists.txt**
```
cmake_minimum_required(VERSION 3.0.0)
project(ConsoleCalculator)

option(CCALC_BUILD_TESTS "Build test programs" ON)

add_subdirectory(src)
if(CCALC_BUILD_TESTS)
    add_subdirectory(tests)
endif()
```

In order to supply this variable to cmake we use the `-D` flag. We will also add a gitlab variable with the same name. We will use the `rules:` clause to control and choose not to run the `test-linux` job at all if the user will set `CCALC_BUILD_TESTS` to any value which is not 1, the default value. The variable is a pipeline variable. It will also be visiable along with its description if we will manually start a gitlab pipeline.

Here are the relevant changes to `.gitlab-ci.yml`

```yml
# ...

variables:
  CCALC_BUILD_TESTS:
    value: 1
    description: Build and run unit tests. Default = true

# ...

build-linux:
  stage: build
  image: $CI_REGISTRY_IMAGE/cpp-from-zero-build-env
  script:
    - mkdir -p build && cd build
    - cmake -DCCALC_BUILD_TESTS=$CCALC_BUILD_TESTS ..

# ...

test-linux:
  variables:
    GIT_STRATEGY: none
  rules:
    - if: $CCALC_BUILD_TESTS == "1"
```

## Lesson 9 : using ccache

Building the same source files over and over again can be quite time consuming. In linux, the `ccache` program is a well known caching utility that can cache compiled objects based on the compilation command and source files and serve them instead of compiling again. Saving precious build time.

To use ccache, we need to install it, lets update our docker file :

**Dockerfile**
```docker
FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y --no-install-recommends make cmake g++ git libgtest-dev ccache

RUN cd /usr/src/gtest && cmake . && make && cp *.a /usr/lib && cd -

ENTRYPOINT /bin/bash
```

We added ccache to our installed programs. To use it, we need to update our cmake file, in this manner:

**CMakeLists.txt**
```cmake
cmake_minimum_required(VERSION 3.0.0)
project(ConsoleCalculator)

option(CCALC_BUILD_TESTS "Build test programs" ON)

find_program(CCALC_CCACHE_FOUND ccache)
if(CCALC_CCACHE_FOUND)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
endif(CCALC_CCACHE_FOUND)

add_subdirectory(src)
if(CCALC_BUILD_TESTS)
    add_subdirectory(tests)
endif()
```

so we used the `find_program` cmake utility to check if ccache is installed. The result is set to the `CCALC_CCACHE_FOUND` variable. If it is installed, we instruct cmake to invoke ccache (actually prepend it) to the compile command that it planned to use.

Lastly, we want to use this in the gitlab CI. there are a few caveat here
* We need to pass the ccache from one build to another. gitlab cache feature is ideal for this. [Here is a page about gitlab cache feature](https://docs.gitlab.com/ee/ci/caching/)
* gitlab build job uses a fresh docker image, which will cause ccache by default to think that the compiler changed. we set the environment variable `CCACHE_COMPILERCHECK=content` to instruct ccache to check the compiler binary itself, when it checks if the compiler changed.
* to pass files from job to job using the gitlab cache, the files should be inside the repository. so we use the `CCACHE_DIR` environment variable to set the cache folder to `.cache` folder under the repository root, where it can be collected as the cache zip result of each job.
* If we really want to make sure that ccache is running, we can run `ccache -s` to print some statistics at the end of the build job.

Here is the resulting `build-linux` job in .gitlab-ci.yml

**.gitlab-ci.yml**
```yml

# ... content ommited

build-linux:
  stage: build
  image: $CI_REGISTRY_IMAGE/cpp-from-zero-build-env
  variables:
    CCACHE_COMPILERCHECK: content
  script:
    - export CCACHE_DIR=${PWD}/.ccache
    - mkdir -p build && cd build
    - cmake -DCCALC_BUILD_TESTS=$CCALC_BUILD_TESTS ..
    - make
    - ccache -s
  artifacts:
    paths:
      - build
  needs:
    - job: "build-linux-build-env"
  cache:
    paths:
      - .ccache

# ... content ommited
```

And here is a sample of ccache -s output, when everything worked well, (and of course while this run is not the first one)

```sh
$ ccache -s
cache directory                     /builds/berenfeldran/cpp-from-zero/.ccache
primary config                      /builds/berenfeldran/cpp-from-zero/.ccache/ccache.conf
secondary config      (readonly)    /etc/ccache.conf
stats zero time                     Sat Apr 10 07:27:44 2021
cache hit (direct)                    12
cache hit (preprocessed)               0
cache miss                             3
cache hit rate                     80.00 %
called for link                        8
cleanups performed                     0
files in cache                         6
cache size                         122.9 kB
max cache size                       5.0 GB
```

## Lesson 10 : use conan for external libraries

With GTest, we were lucky to use an external library that is easy to compile and install. It might not be the case with other libraries. `conan`, now owned by JFrog, is a utility that can ease the use of 3rd party libraries. It has a client server architecture with a client cache 
and a well known server, [conan center](https://conan.io/) that already has many packages ready, GTest included.
Another huge benefit of conan is that it is multi platform. We will use it in our next lectures to build and test our application on windows as well.

To use conan, we have to install it. The best way is via `pip`, which is the python packaging standard utility. Here is our updated Dockerfile

**Dockerfile**
```docker
FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y --no-install-recommends make cmake g++ git libgtest-dev ccache \
    python3 python3-pip python3-setuptools

ENV CC=/usr/bin/gcc
ENV CXX=/usr/bin/g++

ADD requirements.txt .
RUN pip3 install -r requirements.txt

RUN apt-get dist-upgrade -y

ENTRYPOINT /bin/bash
```

So we installed pip and setuptools, and afterwards we installed conan, that is the only requirements currently in the `requirements.txt` file

With conan installed, we need to integrate it into our build procedure. The most important command is `conan install` that will install external libaries specified usually in `conanfile.txt` file, and will build a way for our build system to find the include directories and link libraries. The way can be a cmake file with variables defined, a text file, or for windows a visual studio properties file.

This can be done by running `conan install` from the shell. There is another option which is a conan cmake module that is available. we added it as `conan.cmake` and now we can change our main cmake file to do this:

**CmakeLists.txt**
```cmake
cmake_minimum_required(VERSION 3.0.0)
project(ConsoleCalculator)

option(CCALC_BUILD_TESTS "Build test programs" ON)

include(conan/conan.cmake)
conan_cmake_install(REFERENCE ${CMAKE_SOURCE_DIR}/conan/conanfile.txt
    BUILD missing
    SETTINGS compiler.libcxx=libstdc++11)
include(build/conanbuildinfo.cmake)

find_program(CCALC_CCACHE_FOUND ccache)
if(CCALC_CCACHE_FOUND)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
endif(CCALC_CCACHE_FOUND)

add_subdirectory(src)
if(CCALC_BUILD_TESTS)
    add_subdirectory(tests)
endif()
```

So we include the conan.cmake file, then we run the procedure `conan_cmake_install` which is exactly like conan install, just from within cmake. We give it as a reference our conanfile.txt. The argument `BUILD missing` will instruct conan to build any library for which it does not have, in the conan center, pre built binaries. The last settings `SETTINGS compiler.libcxx=libstdc++11` deals with c++ ABI, that changed in c++11, and must be the same for our code that uses c++11 ABI (with g++ 7.5) and the one for which the library is built on.

with this done, when we configure cmake, a file in the build directory, `build/conanbuildinfo.cmake` is created, and it has a lot of variables we can use in our conan script to include and link our libraries. This is the updated cmake file for the tests

**tests/CMakeLists.txt**
```cmake
# testCalculate testing executable
link_directories(${CONAN_LIB_DIRS_GTEST})
add_executable(testCalculate ${CMAKE_CURRENT_SOURCE_DIR}/testCalculate.cpp)
target_include_directories(testCalculate PRIVATE ${CONAN_INCLUDE_DIRS_GTEST})
target_link_libraries(testCalculate PRIVATE CalculateLib ${CONAN_LIBS_GTEST})
```

conan also exposes variables like `CONAN_LIBS` and `CONAN_INCLUDE_DIRS` in case we have many external libraries and we just some variable that will include and link with all of them.

Using conan is not that easy, but the notable difference here is the amount of effort to add more external libraries. Most of the common c++ libraries are already in conan center, so using them simply involves adding them to the conanfile.txt and adding the relevant variables to the relevant cmake files. conan caching will make sure that nothing is built twice.

Of course, we also want to use the conan cache. Conan store pre-built objects in its cache and can use them to speed up built. The same gitlab cache that we used with ccache can be easily used for the conan cache. We achive this by setting `CONAN_USER_HOME: ${CI_PROJECT_DIR}`, and adding the `.ccache` folder, relative to the repository root, to the gitlab cache.

## Lesson 11 : build on windows

Many software projects, especially desktop applications, are meant to be run on several OS, windows included. So far, we have built the project on a single platform - Ubuntu 18.04 . However we used some utilties that are cross OS and cross platform - namely `cmake` and `conan`. So now it's time to enjoy our investment and see how we can add a build and test jobs on windows to the pipeline.

Gitlab added windows VM machines, hosted on GCP, quite a while ago. They are available with the appropriate runner tags for us, with a reasonable amount of software installed on them. Conan and Cmake however were not installed on this GCP windows VM image. Lucky for us, another software management tool - `choco` from [https://docs.chocolatey.org/en-us/](https://docs.chocolatey.org/en-us/) is avilable, and choco has tons of sofware that can be easily installed from a power shell console, cmake and conan included.

So without further delay, here are the added jobs to the pipeline:

**.gitlab-ci.yml**
```yml

# ... [ content ommited ]

build-windows:
  tags:
    - shared-windows
    - windows
    - windows-1809
  stage: build
  script:
    - choco config set cacheLocation "%CI_PROJECT_DIR%\\.choco"
    - choco install -y cmake
    - $env:Path += ";C:\\Program Files\\CMake\\bin"
    - choco install -y conan
    - $env:Path += ";C:\\Program Files\\Conan\\conan"
    - md build
    - cd build
    - cmake ..
    - cmake --build . --config Release
  artifacts:
    paths:
      - build

# ... [ content ommited ]

test-windows:
  variables:
    GIT_STRATEGY: none
  rules:
    - if: $CCALC_BUILD_TESTS == "1"
  tags:
    - shared-windows
    - windows
    - windows-1809
  stage: test
  script:
    - build/tests/Release/testCalculate.exe --gtest_output=xml:build/tests/Release/testCalculate.xml
  needs:
    - job: "build-windows"
  artifacts:
    when: always
    reports:
      junit: build/tests/Release/testCalculate.xml
```

So a few notes here
* the `shared-windows`, `windows` and `windows-1809` tags are the tags needed to get the GCP VM runner from gitlab
* we set the choco utility cache to the `.choco` folder related to the project dir (where the git repo is cloned). we also added it as a cache path
* after installing cmake and conan we added it to the path so that we can trigger cmake easily (and cmake in turn can trigger conan)
* `cmake --build` is doing for us the build job, instead of checking how to invoke `msbuild.exe` (or maybe `devenv` manually, which is also possible)
* the cmake job here create a solution (`.sln` file) in the build folder. Here, unlike in ubuntu, a developer that would like to work in windows with Visual Studio, will have to generate the solution in the same way. Some projects choose to store the solution and project files in the git repo. Others just keep things this way.
