FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y --no-install-recommends make cmake g++ git libgtest-dev ccache \
    python3 python3-pip python3-setuptools

ENV CC=/usr/bin/gcc
ENV CXX=/usr/bin/g++

ADD requirements.txt .
RUN pip3 install -r requirements.txt

RUN apt-get dist-upgrade -y

ENTRYPOINT /bin/bash
